# OpenCV
OpenCVはComputer VisionのOpen Source Library．C++用のライブラリだが，大半の関数・モジュールはpythonから読み込めるます

##
 * [OpenCV-Python Tutorials’](https://opencv-python-tutroals.readthedocs.org/en/latest/ "OpenCV-Python Tutorials’")
 * [OpenCV 3.0.0](http://docs.opencv.org/3.0.0/ "OpenCV 3.0.0")
 * [OpenCV 3 with Python(英語)](http://www.bogotobogo.com/python/OpenCV_Python/python_opencv3.php "OpenCV 3 with Python")
 * [OpenCV-Pythonのインストール方法(Ubuntu 14.04)](https://charmie11.wordpress.com/2015/11/09/python-2-7-opencv-3-0-0-on-ubuntu-using-anaconda/)
 * [OpenCV-Pythonのインストール方法(Windows 8.1)]( https://charmie11.wordpress.com/2015/06/16/python-2-7-opencv-3-0-0-installation-on-windows/)