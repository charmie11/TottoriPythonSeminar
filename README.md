# 鳥取python勉強会の管理用レポジトリ

* [運営者](http://www.ele.tottori-u.ac.jp/sd/Member/oyamada/ "運営者")
* [スケジュール](./schedule/ "スケジュール")
Googleカレンダー([ICAL](https://calendar.google.com/calendar/ical/parpm8ncq5s6p7q4hqovpb6b88%40group.calendar.google.com/public/basic.ics "ICAL"), [HTML](https://calendar.google.com/calendar/embed?src=parpm8ncq5s6p7q4hqovpb6b88%40group.calendar.google.com&ctz=Asia/Tokyo "HTML"))
* [メーリングリスト](https://groups.google.com/forum/#!forum/tottoripythonseminar "メーリングリスト") <- 勉強会開催の連絡用のメーリングリストです．参加を希望する方はリンクにアクセスしてください．

## 勉強会の目的
pythonを使ってComputer Vision, Pattern Recognition, Machine Learning関連について楽しく勉強

## 次回勉強会
* 日時: 2016年8月29日(月) 13:00-14:30
* 場所: TBD
* 内容: 機械学習 part 2
* 発表者: 安部くん(scikit-learn)，田路くん・小山田(Kerasを使ったDeep Learning)

## 勉強会テーマ候補
* 機械学習(sk-learn)
* データの可視化(pandas, matplotlib, bokeh)
* ディープラーニング(chainer, keras)

## レポジトリの目的
勉強会のスケジュール管理，リクエストの受付，資料管理

## Python開発環境の設定
勉強会参加者で確認した各種OSでのPython開発環境の設定については[ここ](./install/ "ここ")にまとめてあります．

## パッケージ
本勉強会で扱っていく予定のパッケージ一覧です．

下記以外に勉強したいパッケージがあればリクエストしてください．
* [numpy](./numpy/ "numpy")(数値計算)
* [matplotlib](./matplotlib/ "matplotlib")(データの可視化)
* [scipy](./scipy/ "scipy")(高水準の科学技術計算)
* [pandas](./pandas/ "pandas")(データ統計処理)
* [scikit-learn](./scikit-learn/ "scikit-learn")(機械学習)
* [opencv](./opencv/ "opencv")(Computer Vision, 画像処理)
* [tensorflow](./tensorflow/ "tensorflow")(深層学習(Deep Learning))

## 勉強会のログ
* 第1回: 2015年12月2日(水): [鳥取python勉強会爆誕 ](http://www.slideshare.net/charmie11/python-1-58647715)
* 第2回: 2016年1月13日(水): [pythonの開発環境の設定](http://www.slideshare.net/charmie11/python-1-58647610)
* 第3回: 2016年2月24日(水): 卒研でこんなプログラミング言語，ライブラリ，ツールボックス使ったよ part 1
* 第4回: 2016年3月7日(月): 卒研でこんなプログラミング言語，ライブラリ，ツールボックス使ったよ part 2
* 第5回: 2016年3月17日(木): 卒研でこんなプログラミング言語，ライブラリ，ツールボックス使ったよ part 3
* 第6回: 2016年5月11日(水): 卒研でこんなプログラミング言語，ライブラリ，ツールボックス使ったよ part 4 + OpenCV-Python使ってみました
* 第7回: 2016年6月8日(水): 可視化(Matplotlibを使ったヒートマップ， [対話的可視化ツール bokeh](http://www.slideshare.net/charmie11/tottori-python-seminar-20160608))
* 第8回: 2016年7月19日(火): 機械学習(Chainerのインストール，Kerasを使った算術演算の実装)