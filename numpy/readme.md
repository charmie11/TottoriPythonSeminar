# Numpy
数値計算用パッケージ

## 
* [Numpy公式(英語)](http://www.numpy.org/ "Numpy公式(英語)")
* [Numpy入門](http://turbare.net/transl/scipy-lecture-notes/intro/numpy/index.html "Numpy入門")
* [NumPy入門](http://rest-term.com/archives/2999/ "NumPy入門")
* [チュートリアル(英語)](https://docs.scipy.org/doc/numpy-dev/user/quickstart.html "チュートリアル(英語)")
* [チュートリアル(和訳)](http://naoyat.hatenablog.jp/entry/2011/12/29/021414 "チュートリアル(和訳)")
* []( "")
