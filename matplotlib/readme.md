# MatPlotLib
可視化ツール

## 
* [公式(英語)](http://matplotlib.org/ "公式(英語)")
* [チュートリアル](http://turbare.net/transl/scipy-lecture-notes/intro/matplotlib/matplotlib.html "チュートリアル")
* [簡単な入門](http://bicycle1885.hatenablog.com/entry/2014/02/14/023734 "簡単な入門")
* [pyplot(英語)](http://matplotlib.org/api/pyplot_api.html "pyplot(英語)")
* [サンプル集](http://kaiseki-web.lhd.nifs.ac.jp/wiki/index.php/Matplotlib_%E3%82%B5%E3%83%B3%E3%83%97%E3%83%AB%E9%9B%86 "サンプル集")
* []( "")