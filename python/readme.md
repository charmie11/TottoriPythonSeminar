# Python

## Python実装ガイド
* [PEP 8(英語)](https://www.python.org/dev/peps/pep-0008/ "PEP 8(英語)")
* [PEP 8(和訳)](https://github.com/mumumu/pep8-ja "PEP 8(和訳)")
* [PEP 257(英語)](https://www.python.org/dev/peps/pep-0257/ "PEP 257Google(英語)")
* [Google(英語)](https://google.github.io/styleguide/pyguide.html "Google(英語)")
* [Google(和訳)](http://works.surgo.jp/translation/pyguide.html "Google(和訳)")

## 
* [Python入門](http://turbare.net/transl/scipy-lecture-notes/intro/language/python_language.html "Python入門")
* [Python Japan](http://www.python.jp/ "Python Japan")
  * [Python 3.4.3](http://docs.python.jp/3/ "Python 3.4.3")
  * [Python 3.3.6](http://docs.python.jp/3.3/ "Python 3.3.6")
  * [Python 2.7ja1](http://docs.python.jp/2.7/ "Python 2.7ja1")
* [A Byte of Python(英語)](http://www.swaroopch.com/notes/python/ "A Byte of Python(英語)")
* []( "")
